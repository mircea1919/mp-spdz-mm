cd python-scripts/
python3 preprocess_query1.py $1 $2
cd ..
if [ $2 == 2 ]
then
  ./compile.py mm_query1_p2
  Scripts/mascot.sh mm_query1_p2 -OF Player-Data/query_one_output
elif [ $2 == 3 ]
then
  ./compile.py mm_query1_p3
  Scripts/$3.sh mm_query1_p3 -OF Player-Data/query_one_output
fi
cd python-scripts/
python3 postprocess_query1.py

