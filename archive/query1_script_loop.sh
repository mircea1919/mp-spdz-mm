for i in 2 4 8 16 32 64 128
do
  cd python-scripts/
  time python3 preprocess_query1.py $i $1
  cd ..
  if [ $1 == 2 ]
  then
    ./compile.py mm_query_one
    Scripts/mascot.sh mm_query_one -OF Player-Data/query_one_output
  elif [ $1 == 3 ]
  then
    ./compile.py mm_query1_p3
    Scripts/$2.sh mm_query1_p3 -OF Player-Data/query_one_output
  fi
  cd python-scripts/
  time python3 postprocess_query1.py
  cd ..
done



