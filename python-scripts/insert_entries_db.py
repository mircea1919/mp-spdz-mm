import random
import mysql.connector
import sys
sys.path.append('../')
import global_variables as gv

number_of_entries = int(sys.argv[1])
db_name = sys.argv[2]

# db_name = "smcql0"

def insert_dummy_diagnose(diagnoses, patients, db_client):
    cursor = db_client.cursor()
    insert_query = "INSERT into " + db_name + ".diagnoses (patient_id, site, year, visit_no, encounter_id, diag_src, icd9, primary_, timestamp_," + \
                   " clean_icd9, major_icd9) VALUES (" + str(random.choices(patients)[0]) + ", 0, 2020, 1, 1, " \
                   + str(random.choices(diagnoses)[0]) +  ", 0, 0, 0, 0, 0)";
    # insert_query = "INSERT into " + db_name + ".diagnoses (patient_id, site, year, visit_no, encounter_id, diag_src, icd9, primary_, timestamp_," + \
    #                " clean_icd9, major_icd9) VALUES (" + str(random.choices(patients)[0]) + ", 0, 2020, 1, 1, " \
    #                + str(random.choices(diagnoses)[0]) + ", 0, 0, " + "'0000-00-00 00:00:00'" + ", 0, 0)";
    # insert_query = "INSERT into " + db_name + ".diagnoses (patient_id, site, year, visit_no, encounter_id, diag_src, icd9, primary_," + \
    #                " clean_icd9, major_icd9) VALUES (" + str(random.choices(patients)[0]) + ", 0, 2020, 1, 1, " \
    #                + str(random.choices(diagnoses)[0]) + ", 0, 0, 0, 0)";
    cursor.execute(insert_query)
    db_client.commit()
    cursor.close()

random_diagnoses = []
random_patiens = []
range_diag = range(11, 40)
range_patients = range(101, 130)
for i in range_diag:
  random_diagnoses.append(i)

for i in range_patients:
  random_patiens.append(i)

db_client = mysql.connector.connect(
        host=gv.HOST,
        user=gv.USER,
        password=gv.PASSWORD,
        database=db_name
    )

for i in range(number_of_entries):
    insert_dummy_diagnose(random_diagnoses, random_patiens, db_client)
db_client.close()
