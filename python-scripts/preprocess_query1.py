import os
import mysql.connector
import sys
from datetime import datetime
sys.path.append('../')
import global_variables as gv
nr_of_inputs = int(sys.argv[1])
nr_of_parties = int(sys.argv[2])
security_model = sys.argv[3]
for_loop_type = False

# if len(sys.argv) > 4:
#     for_loop_type = sys.argv[4]


print("\n\n\n #########################################################")
print("Date and time:" + str(datetime.now()))
print("Number of inputs: " + str(nr_of_inputs))
print("Number of parties: " + str(nr_of_parties))
# print("Range Opt: " + str(for_loop_type))

print("\n")

files_array = []
db_array = []

for i in range(nr_of_parties):
    files_array.append("Player-Data/Input-P" + str(i) + "-0")
    db_array.append("smcql" + str(i))


# todo: error handling; must be int; must be offered!
inputs_per_party = int(nr_of_inputs / nr_of_parties)

select_query = "SELECT diag_src FROM diagnoses LIMIT " + str(inputs_per_party)
count_query = "SELECT count(*) FROM diagnoses"

def get_diag_and_count(db_name, filename):
    db_client = mysql.connector.connect(
        host=gv.HOST,
        user=gv.USER,
        password=gv.PASSWORD,
        database=db_name
    )
    cursor = db_client.cursor()
    cursor.execute(select_query)
    diagnoses = cursor.fetchall()
    cursor.execute(count_query)
    cnt = cursor.fetchone()

    if int(cnt[0]) < inputs_per_party:
        count = int(cnt[0])
    else:
        count = inputs_per_party

    file = gv.DIRECTORY + filename
    os.remove(file)
    f = open(file, "a")
    # if for_loop_type != "range_opt":
    f.write(str(count) + " ")
    for row in diagnoses:
        f.write(str(row[0]) + " ")
    f.close()

    # todo: to be updated in case the file name changes!
    # todo: move out of the loop!

    # if (for_loop_type == 'range_opt'):
    if (nr_of_parties == 2):
        file2 = gv.DIRECTORY + "Programs/Public-Input/mm_query1_p2_range_opt"
    elif (nr_of_parties == 3):
        file2 = gv.DIRECTORY + "Programs/Public-Input/mm_query1_p3_range_opt"
    elif (nr_of_parties == 4):
        file2 = gv.DIRECTORY + "Programs/Public-Input/mm_query1_p4_range_opt"
    elif (nr_of_parties == 5):
        file2 = gv.DIRECTORY + "Programs/Public-Input/mm_query1_p5_range_opt"
    elif (nr_of_parties == 6):
        file2 = gv.DIRECTORY + "Programs/Public-Input/mm_query1_p6_range_opt"
    elif (nr_of_parties == 8):
        file2 = gv.DIRECTORY + "Programs/Public-Input/mm_query1_p8_range_opt"
    elif (nr_of_parties == 10):
        file2 = gv.DIRECTORY + "Programs/Public-Input/mm_query1_p10_range_opt"
    f = open(file2, "a")
    os.remove(file2)
    f = open(file2, "a")
    f.write(str(count))
    f.close()


for j in range(nr_of_parties):
    get_diag_and_count(db_array[j], files_array[j])