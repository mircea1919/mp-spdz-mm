import sys
sys.path.append('../')
import global_variables as gv

file_input = gv.DIRECTORY + "Player-Data/query_one_output-P0-0"
my_dict = dict()

with open(file_input) as f:
    lines = f.readlines()

    for line in lines:
        line_tupple = line.strip().split(", ")
        if line_tupple[0] not in my_dict:
            my_dict[line_tupple[0]] = line_tupple[1]

for index, (key, value) in enumerate(my_dict.items()):
    # limit of the query is 10
    if int(index + 1) > 10:
        break
    print("There are " + value + " patients with diagnose " + key)

print("\n\n #########################################################")

