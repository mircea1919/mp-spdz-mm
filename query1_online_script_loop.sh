for i in 2 4 8 16 32 64 128 256 512 1024 2048 4096 8192 16384 32768
do
  cd python-scripts/
  time python3 preprocess_query1.py $i $1 $2 $3
  cd ..
  if [ $1 == 2 ]
  then
    ./compile.py -F 64 mm_query1_p2_range_opt
    Scripts/$2.sh mm_query1_p2_range_opt -F -OF Player-Data/query_one_output
  elif [ $1 == 3 ]
  then
    ./compile.py -F 64 mm_query1_p3_range_opt
    Scripts/$2.sh mm_query1_p3_range_opt -F -OF Player-Data/query_one_output
  fi
  cd python-scripts/
  time python3 postprocess_query1.py
  cd ..
done



