from Compiler.library import *
from Compiler.types import *

data = Array(100, sint)
sum = Array(100, sint)
limit = 10

data.assign_all(sint(0))
sum.assign_all(sint(0))

count1 = sint.get_input_from(0).reveal()
count2 = sint.get_input_from(1).reveal()
count = count1 + count2

@for_range(count1)
def _(i):
    in1 = sint.get_input_from(0)
    data[i] = in1


@for_range(count2)
def _(j):
    in2 = sint.get_input_from(1)
    data[count1 + j] = in2

temp = Array(2, sint)
@for_range(count)
def _(i):
    @for_range(count)
    def _(j):
        incr = data[i].equal(data[j])
        sum[i] += incr

@for_range(count-1)
def _(i):
    @for_range(count-i-1)
    def _(j):
        check = sum[j+1].greater_than(sum[j])
        temp[0] = data[j] * (1 - check) + data[j+1] * check
        temp[1] = data[j] * check + data[j+1] * (1 - check)
        data[j] = temp[0]
        data[j+1] = temp[1]

        temp[0] = sum[j] * (1 - check) + sum[j+1] * check
        temp[1] = sum[j] * check + sum[j+1] * (1 - check)
        sum[j] = temp[0]
        sum[j+1] = temp[1]


@for_range(count)
def _(i):
    print_ln("%s, %s", data[i].reveal(), sum[i].reveal())



