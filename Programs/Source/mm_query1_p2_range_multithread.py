from Compiler.library import *
from Compiler.types import *

# we need also Input-P0-1 and Input-P0-2

# error thrown:
# terminate called after throwing an instance of 'closed_connection'
# terminate called recursively
# https://github.com/data61/MP-SPDZ/issues/163
# closed_connection is thrown when the other side aborts unexpectedly.


sfix.set_precision(16, 32)

data = Array(100, sfix)
sum = Array(100, sfix)
limit = 10

sum.assign_all(sfix(0))

count1 = 8
count2 = 8
count = count1 + count2

data_two = Matrix(3, 2, sfix)

nr_of_threads = 1
nr_parallel = 2

# @for_range_opt_multithread(nr_of_threads, count1)
# @for_range_opt(count1)
@for_range_parallel(nr_parallel, count1)
def _(i):
    in1 = sfix.get_input_from(0)
    data[i] = in1
#

## for_range_parallel -> not so much time difference

# @for_range_opt_multithread(nr_of_threads, count2)
# @for_range_opt(count2)
@for_range_parallel(nr_parallel, count2)
def _(j):
    in2 = sfix.get_input_from(1)
    data[count1 + j] = in2
#
temp = Array(2, sfix)
@for_range_opt(count)
def _(i):
    @for_range_opt(count)
    def _(j):
        incr = (data[i] == data[j])
        sum[i] += incr
#
@for_range_opt(count-1)
def _(i):
    @for_range_opt(count-i-1)
    def _(j):
        check = (sum[j+1] > (sum[j]))
        temp[0] = data[j] * (1 - check) + data[j+1] * check
        temp[1] = data[j] * check + data[j+1] * (1 - check)
        data[j] = temp[0]
        data[j+1] = temp[1]

        temp[0] = sum[j] * (1 - check) + sum[j+1] * check
        temp[1] = sum[j] * check + sum[j+1] * (1 - check)
        sum[j] = temp[0]
        sum[j+1] = temp[1]
#
# @for_range_opt_multithread(nr_of_threads, count)
@for_range_opt(count)
def _(i):
    print_ln("%s, %s", data[i].reveal(), sum[i].reveal())