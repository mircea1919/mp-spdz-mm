from Compiler.library import *
from Compiler.types import *

data = Array(50000, sfix)
sum = Array(50000, sfix)
limit = 10

data.assign_all(sfix(0))
sum.assign_all(sfix(0))

count_input = public_input()
count = count_input * 3

#todo: create a function for this loop to make sure it works!
@for_range_opt(count_input)
def _(i):
    in1 = sfix.get_input_from(0)
    data[i] = in1


@for_range_opt(count_input)
def _(j):
    in2 = sfix.get_input_from(1)
    data[count_input + j] = in2

@for_range_opt(count_input)
def _(k):
    in3 = sfix.get_input_from(2)
    data[count_input + count_input + k] = in3

temp = Array(2, sfix)
@for_range_opt(count)
def _(i):
    @for_range(count)
    def _(j):
        incr = (data[i] == data[j])
        sum[i] += incr

@for_range(count-1)
def _(i):
    @for_range(count-i-1)
    def _(j):
        check = (sum[j+1] > sum[j])
        temp[0] = data[j] * (1 - check) + data[j+1] * check
        temp[1] = data[j] * check + data[j+1] * (1 - check)
        data[j] = temp[0]
        data[j+1] = temp[1]

        temp[0] = sum[j] * (1 - check) + sum[j+1] * check
        temp[1] = sum[j] * check + sum[j+1] * (1 - check)
        sum[j] = temp[0]
        sum[j+1] = temp[1]


@for_range(count)
def _(i):
    print_ln("%s, %s", data[i].reveal(), sum[i].reveal())



