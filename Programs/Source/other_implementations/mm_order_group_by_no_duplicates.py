from Compiler.library import *
from Compiler.types import *

#print_ln('Party 0: please input count then numbers')
#print_ln('Party 1: please input count then numbers')

data = Array(100, sint)
sum = Array(100, sint)
limit = 10

data.assign_all(sint(0))
sum.assign_all(sint(0))

count1 = sint.get_input_from(0).reveal()
count2 = sint.get_input_from(1).reveal()
count = count1 + count2


@for_range(count1)
def _(i):
    in1 = sint.get_input_from(0)
    data[i] = in1


@for_range(count2)
def _(j):
    in2 = sint.get_input_from(1)
    data[count1 + j] = in2

temp = Array(2, sint)
@for_range(count)
def _(i):
    @for_range(count)
    def _(j):
        incr = data[i].equal(data[j])
        sum[i] += incr


## we can still consider if it is not more efficient to do the ordering in post processing
@for_range(count-1)
def _(i):
    @for_range(count-i-1)
    def _(j):
        check = sum[j].greater_than(sum[j+1])
        temp[0] = data[j] * (1 - check) + data[j+1] * check
        temp[1] = data[j] * check + data[j+1] * (1 - check)
        data[j] = temp[0]
        data[j+1] = temp[1]

        temp[0] = sum[j] * (1 - check) + sum[j+1] * check
        temp[1] = sum[j] * check + sum[j+1] * (1 - check)
        sum[j] = temp[0]
        sum[j+1] = temp[1]


## todo: save in a csv
# file = "myfile.txt"
# file = "/home/mm/thesis/git/mp-spdz-mm/Player-Data/Public-Output-0"
# os.remove(file)
# f = open(file, "w")

print_ln("%s, %s", data[0].reveal(), sum[0].reveal())

@for_range(count-1)
def _(i):

    temp[0] = data[i]
    # temp[1] = sum[0]
    # check for limit 10
    # we can hide the number of diagnoses of the patients which have been diagnosed more than 10 times
    # we cannot hide the number of patients which were diagnoses more than 10 times
    check = sint(limit).greater_than(sum[i+1])
    sum[i+1] = check*sum[i+1]

    check_limit = sum[i+1].greater_than(sint(0))
    check_previous = data[i+1].equal(temp[0])

    # print_ln("teeeeest: %s, %s, %s, %s", temp[0].reveal(), data[i+1].reveal(), check_previous.reveal(), sum[i+1].reveal())

    # temp[1] = (1-check_previous)*sum[i+1]
    # sum[i+1] = temp[1]
    @if_ (check_limit.reveal())
    def _():

        ##todo: patient_id should not be revealed
        print_ln("%s, %s", data[i+1].reveal(), sum[i+1].reveal())


## saved secret data in Persistence/ folder; everything is encrypted
# sint.write_to_file(sum)

## I do not understand how write_to_socket works
# cint.write_to_socket(regint(1), Array(100, cint))


