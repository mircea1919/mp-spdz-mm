from Compiler.library import *
from Compiler.types import *

print_ln('Party 0: please insert multiple rows')
print_ln('Party 1: please insert multiple rows')


##### ex input0 file:
#6 12 31 4
#12 6 11 1
#2 5 30 17
#5 10 11 5
#10 2 11 4
######### ex input1 file:
#16 22 1 22
#2 16 111 10
#32 52 3 7
#11 45 3 1
#43 2 111 0
####
## add order attribute:
#echo 2 > Programs/Public-Input/mm_group_by_matrix
####

order_attr = public_input()

## TODO: create function for creating the matrix
## TODO: set params dynamically
m = Matrix(10, 4, sfix)
@for_range_opt(5)
def _(row):
    for column in range(4):
        m[row][column] = sfix.get_input_from(0)


@for_range_opt(5)
def _(row):
    for column in range(4):
        m[5+row][column] = sfix.get_input_from(1)

s = Matrix(10, 2, sfix)
@for_range(10)
def _(i):
    s[i][0] = 0

temp = Array(1,cint)
@for_range(10)
def _(i):
    temp[0] = 0
    @for_range(10)
    def _(j):
        @if_((s[j][0] == m[i][order_attr]).reveal())
        # @if_e((s[j][0] == m[i][order_attr]).reveal())
        def _():
            s[j][1] = s[j][1] + sfix(1)
            temp[0] = 1

        # the else does not work properly for some reason
        @if_((s[j][0] != m[i][order_attr]).reveal() & (s[j][0] == 0).reveal() & (((j>0).reveal() | (j==0).reveal())) & (temp[0] == 0))
        # @else_((s[j][0] == 0).reveal() & (((j>0).reveal() | (j==0).reveal())))
        def _():
            @if_((s[j - 1][0] != 0).reveal() & (s[j - 1][0] != m[i][order_attr]).reveal())
            def _():
                s[j][0] = m[i][order_attr]
                s[j][1] = sfix(1)

@for_range(10)
def _(j):
    print_ln("Matrix attribute %s", m[j][order_attr].reveal())
    print_ln('check here: %s, %s', s[j][0].reveal(), s[j][1].reveal())
