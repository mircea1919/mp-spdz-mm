from Compiler.library import *
from Compiler.types import *

print_ln('Party 0: please insert multiple rows')
print_ln('Party 1: please insert multiple rows')
#print_ln('Party 2: ....')
## we can only use 2 parties!!


##### ex input0 file:
#6 12 31 4
#12 6 11 1
#2 5 30 17
######### ex input1 file:
#16 22 1 22
#2 16 111 10
#32 52 3 7
####
## add order attribute:
#echo 1 > Programs/Public-Input/mm_order_by_matrix
####

order_attr = public_input()
m = Matrix(6, 4, sfix)
@for_range_opt(3)
def _(row):
    for column in range(4):
        m[row][column] = sfix.get_input_from(0)

@for_range_opt(3)
def _(row):
    for column in range(4):
        m[3+row][column] = sfix.get_input_from(1)

print_ln('result: %s, %s, %s, %s, %s', m[0][order_attr].reveal(),
         m[1][order_attr].reveal(), m[2][order_attr].reveal(),
         m[3][order_attr].reveal(), m[4][order_attr].reveal())


## todo: make this an array
s = Matrix(1, 4, sfix)
s[0][0] = sfix(0)
s[0][1] = sfix(0)
s[0][2] = sfix(0)
s[0][3] = sfix(0)

@for_range(6)
def _(j):
    @for_range(5)
    def _(i):
        print_ln("teeest: %s, %s, %s, %s", i, m[i][order_attr].reveal(),
                 m[i+1][order_attr].reveal(), s[0][order_attr].reveal())
        @if_((m[i][order_attr]>m[i+1][order_attr]).reveal())
        def _():
            s[0][0] = m[i+1][0]
            s[0][1] = m[i+1][1]
            s[0][2] = m[i+1][2]
            s[0][3] = m[i+1][3]
            m[i+1][0] = m[i][0]
            m[i+1][1] = m[i][1]
            m[i+1][2] = m[i][2]
            m[i+1][3] = m[i][3]
            m[i][0] = s[0][0]
            m[i][1] = s[0][1]
            m[i][2] = s[0][2]
            m[i][3] = s[0][3]

print_ln('result: %s, %s, %s, %s, %s', m[0][order_attr].reveal(),
         m[1][order_attr].reveal(), m[2][order_attr].reveal(),
         m[3][order_attr].reveal(), m[4][order_attr].reveal())




