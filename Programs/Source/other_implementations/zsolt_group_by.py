from Compiler.library import *
from Compiler.types import *

print_ln('Party 0: please input count then numbers')
print_ln('Party 1: please input count then numbers')

# Here the computation maintains privacy of the “group values”.
# If you add a third party, it cannot infer what is the individual count of the others.

data = Array(100, sint)
sum = Array(100, sint)

data.assign_all(sint(0))
sum.assign_all(sint(0))

count1 = sint.get_input_from(0).reveal()
count2 = sint.get_input_from(1).reveal()
count = count1 + count2


@for_range(count1)
def _(i):
    in1 = sint.get_input_from(0)
    data[i] = in1


@for_range(count2)
def _(j):
    in2 = sint.get_input_from(1)
    data[count1 + j] = in2


@for_range(count)
def _(i):
    @for_range(count)
    def _(j):
        incr = data[i].equal(data[j])
        sum[i] += incr


@for_range(count1)
def _(i):
    print_ln_to(0, 'group count of input ID %s is %s', i, sum[i].reveal_to(0))


@for_range(count2)
def _(j):
    print_ln_to(1, 'group count of input ID %s is %s', j, sum[count1 + j].reveal_to(1))