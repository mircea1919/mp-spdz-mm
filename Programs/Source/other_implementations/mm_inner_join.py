from Compiler.library import *
from Compiler.types import *

print_ln('Party 0: please insert multiple rows')
print_ln('Party 1: please insert multiple rows')

#######################
# 100 11 12 13
# 200 21 22 23
# 300 31 32 33
# 400 41 42 43
# 500 51 52 53
#
# 14 100 15 16
# 34 300 35 36
# 17 100 18 19
# 41 400 42 43
# 37 300 38 39
###################

row_one = 5
row_two = 5
col = 4

col_one_join = 0
col_two_join = 1

m_one = Matrix(row_one, col, sfix)
m_two = Matrix(row_two, col, sfix)
m = Matrix(row_one + row_two, col+col, sfix)

m.assign_all(sfix(0))
@for_range_opt(row_one)
def _(row):
    for column in range(col):
        m_one[row][column] = sfix.get_input_from(0)

@for_range_opt(row_two)
def _(row):
    for column in range(4):
        m_two[row][column] = sfix.get_input_from(1)

@for_range_opt(row_one)
def _(i):
    @for_range_opt(row_two)
    def _(j):
        @if_((m_one[i][col_one_join] == m_two[j][col_two_join]).reveal())
        def _():
            print_ln('')
            @for_range(col)
            def _(k):
                print_str_if(k!=0 ,'%s, ', m_one[i][k].reveal())
                print_str('%s, ', m_two[j][k].reveal())