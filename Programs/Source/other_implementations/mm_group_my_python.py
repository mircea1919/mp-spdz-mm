N = 10
M = 4
res = [[0 for i in range(M)] for j in range(N)]

res[0][1] = 22
res[1][1] = 9
res[2][1] = 12
res[3][1] = 9
res[4][1] = 22
res[5][1] = 1
res[6][1] = 22
res[7][1] = 12
res[8][1] = 22
res[9][1] = 9

s = [] # tupples are immutable so they do not work

for i in range(0, N-1):
    temp = 0
    for j in range(0, len(s)):
        if s[j][0] == res[i][1]:
            aux = s[j][1]
            s[j][1] = aux + 1
            temp = 1
            break
    if temp == 0:
        s.append((res[i][1], 1))

print(s)
