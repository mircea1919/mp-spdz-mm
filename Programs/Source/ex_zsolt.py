from Compiler.library import *
from Compiler.types import *

print_ln('Party 0: please input count then numbers')

print_ln('Party 1: please input count then numbers')

data = Array(100, sint)

data.assign_all(sint(0))

count1 = sint.get_input_from(0).reveal()

count2 = sint.get_input_from(1).reveal()

count = count1 + count2


@for_range(count1)
def _(j):
    in1 = sint.get_input_from(0)

    data[in1.reveal()] += sint(1)


@for_range(count2)
def _(j):
    in2 = sint.get_input_from(1)

    data[in2.reveal()] += sint(1)


@for_range(100)
def _(j):
    rev = data[j].reveal()

    @if_(rev > 0)
    def _():
        print_ln('Group id %s has count %s', j, rev)