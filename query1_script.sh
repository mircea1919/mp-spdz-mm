cd python-scripts/
python3 preprocess_query1.py $1 $2 $3
cd ..
if [ $2 == 2 ]
then
  time ./compile.py mm_query1_p2_range_opt
  Scripts/$3.sh mm_query1_p2_range_opt -OF Player-Data/query_one_output
elif [ $2 == 3 ]
then
  ./compile.py mm_query1_p3_range_opt
  Scripts/$3.sh mm_query1_p3_range_opt -OF Player-Data/query_one_output
fi
cd python-scripts/
python3 postprocess_query1.py

