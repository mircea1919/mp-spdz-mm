for i in 1024 2048
do
  cd python-scripts/
  time python3 preprocess_query1.py $i $1 $2
  cd ..
  if [ $1 == 2 ]
  then
    ./compile.py mm_query1_p2_range_opt
    Scripts/$2.sh mm_query1_p2_range_opt -OF Player-Data/query_one_output
  elif [ $1 == 3 ]
  then
    ./compile.py mm_query1_p3_range_opt
    Scripts/$2.sh mm_query1_p3_range_opt -OF Player-Data/query_one_output
  fi
  cd python-scripts/
  time python3 postprocess_query1.py
  cd ..
done



