import sys
import global_variables as gv

data = []
limit = 10

count = int(sys.argv[1])
parties = int(sys.argv[2])
count_input = int(count / parties)+1

sum = []
for i in range(count):
    sum.append(0)

for i in range(parties):
    file_input = gv.DIRECTORY + "Player-Data/Input-P" + str(i) + "-0"

    with open(file_input) as f:
        numbers_string = f.readlines()
        numbers = numbers_string[0].strip().split(" ")

    for j in range(count_input):
        data.append(numbers[j])

for i in range(count):
    for j in range(count):
        if data[i] == data[j]:
            sum[i] = sum[i]+1

temp = [0, 0]
for i in range(count-1):
    for j in range(count-i-1):
        check = (sum[j+1] > sum[j])
        temp[0] = data[j] * (1 - check) + data[j+1] * check
        temp[1] = data[j] * check + data[j+1] * (1 - check)
        data[j] = temp[0]
        data[j+1] = temp[1]

        temp[0] = sum[j] * (1 - check) + sum[j+1] * check
        temp[1] = sum[j] * check + sum[j+1] * (1 - check)
        sum[j] = temp[0]
        sum[j+1] = temp[1]

returned_elements = {}

for i in range(count):
    if len(returned_elements.keys()) < 10:
        if data[i] not in returned_elements:
            returned_elements[data[i]] = sum[i]
    else:
        continue

print("First " + str(limit) + " most popular diagnostics are: " + str(returned_elements))